import os
import json
from git import Repo

data = {}

path = "./gsoc_2020/data_test/SoftStackCatalog/modulefiles/mugqic/"

def parse_dir(path):
    """
        path: A string containing the path to the repo
    """

    for root, dirs, files in os.walk(path):
        path = root.split(os.sep)
        versions = []
        for file in files:
            if(file != '.version'):
                versions.append(file)
        if(os.path.basename(root) != ''):
            data[os.path.basename(root)] = {
                'available_verisons' : versions
            }

    with open('result.json', 'w') as fp:
        json.dump(data, fp, indent=4)
    print("Done")



if(os.path.exists(path)):
    parse_dir(path)
else:
    Repo.clone_from('https://bitbucket.org/mugqic/gsoc_2020', 'gsoc_2020')
    if(os.path.exists(path)):
        parse_dir(path)
